﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Word = Microsoft.Office.Interop.Word;
using addin_inv_word2._invest_uceDataSetTableAdapters;
using static addin_inv_word2._invest_uceDataSet;
using System.Drawing;

namespace addin_inv_word2
{
    public partial class Investigacion
    {
        Dictionary<int, EstilosRow> estilo;
        Dictionary<int, TablaEstiloRow> estilosTablas;
        Dictionary<int, TablaPlantillaRow> plantillasTablas;


        private void Investigacion_Load(object sender, RibbonUIEventArgs e)
        {
            //Mostrar_form_seleccion_Invest();
            FormTablas_FormClosed(null, null);
        }


        /// <summary>
        /// Metodo generico q usa el parametro ESTILO para formatear la seleccion actual
        /// </summary>
        /// <param name="estilo"></param>
        private void Do_formatear(EstilosRow estilo)
        {
            try
            {
                if (estilo != null)
                {
                    var sel = Globals.ThisAddIn.Application.Selection;
                    sel.Range.Font.Name = estilo.font;
                    sel.Range.Font.Bold = estilo.isBold ? 1 : 0;
                    sel.Range.Font.Italic = estilo.isCursiva ? 1 : 0;
                    sel.Range.Font.Size = estilo.fontSize;
                    Color c = ColorTranslator.FromHtml(estilo.color);

                    sel.Range.Font.Color = (Word.WdColor)(c.R + 0x100 * c.G + 0x10000 * c.B);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("error al formatear Seleccione Investigador.", "Error!");
                    Mostrar_form_seleccion_Invest();
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("error al formatear:" + ex.Message, "Error!");
            }
        }

        private void Investigadores_Click(object sender, RibbonControlEventArgs e)
        {
            FormInvestigadores f = new FormInvestigadores();
            f.FormClosed += F_FormClosed;
            f.Show();
        }

        private void SelectInvest_Click(object sender, RibbonControlEventArgs e)
        {
            Mostrar_form_seleccion_Invest();
        }

        private void Mostrar_form_seleccion_Invest()
        {
            forms.FormSelectInvest f = new forms.FormSelectInvest();
            f.FormClosed += F_FormClosed;
            f.Show();
        }
        /// <summary>
        /// Cuando ya se cierra el formulario de seleccion de investigador
        /// este metodo actualiza el label q muestra el investigador seleccionado asi como el ESTILO a aplicar
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void F_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            try
            {
                int idInvest = Globals.ThisAddIn.IdInvest;
                if (idInvest != 0)
                {
                    InvestigadoresTableAdapter daInvest = new InvestigadoresTableAdapter();
                    EstilosTableAdapter daEstilos = new EstilosTableAdapter();

                    InvestigadoresDataTable dt = daInvest.GetDataByInvestID(idInvest);
                    EstilosDataTable dte = daEstilos.GetDataByInvestigadorId(idInvest);
                    if (dt.Rows.Count > 0 && dte.Rows.Count > 0)
                    {
                        var inv = dt.Rows[0]["Id"];
                        var nom = dt.Rows[0]["nombre"];
                        SelectedInvest.Label = nom.ToString();
                        //vaciamos los estilos anteroires siesq hay
                        this.estilo = new Dictionary<int, EstilosRow>();
                        estilo.Clear();
                        menu1.Dynamic = true;
                        menu1.Items.Clear();
                        // los nuevos estilos los ponemos en el diccionario
                        foreach (EstilosRow item in dte.Rows)
                        {
                            //creamos el boton
                            RibbonButton button = this.Factory.CreateRibbonButton();
                            button.Label = item.key;
                            button.Tag = item.Id;
                            button.Click += new RibbonControlEventHandler(Estilo_button_click);
                            estilo.Add(item.Id, item);
                            menu1.Items.Add(button);
                        }

                    }
                    else
                    {
                        throw new Exception("No se encontró investigador");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("error al seleccionar investigador", "Error!");
            }
        }
        /// <summary>
        /// CUando se da click en formatear q es un boton se ejecuta este metodo
        /// aqui se determina cual se dio click mediante el TAG sabemos el ID del estilo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Estilo_button_click(object sender, RibbonControlEventArgs e)
        {

            var btn = (RibbonButton)sender;
            var id = (int)(btn.Tag);
            Do_formatear(estilo[id]);
        }

        //ORIENTACION
        private void CambioOrientacion_Click(object sender, RibbonControlEventArgs e)
        {
            var sel = Globals.ThisAddIn.Application.Selection;

            var landscape = sel.Range.Sections.PageSetup.Orientation == Word.WdOrientation.wdOrientLandscape;

            sel.Range.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            sel.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            sel.Range.Sections.PageSetup.Orientation = landscape ? Word.WdOrientation.wdOrientPortrait : Word.WdOrientation.wdOrientLandscape;
            sel.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            sel.Range.Sections.PageSetup.Orientation = landscape ? Word.WdOrientation.wdOrientLandscape : Word.WdOrientation.wdOrientPortrait;
        }
        //TABLAS
        private void Tablas_Click(object sender, RibbonControlEventArgs e)
        {
            FormTablas formTablas = new FormTablas();
            formTablas.FormClosed += FormTablas_FormClosed;
            formTablas.Show();
        }

        private void FormTablas_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            //--ESTILOS TABLAS
            //vaciamos los estilos anteroires siesq hay
            this.estilosTablas = new Dictionary<int, TablaEstiloRow>();
            FormatearTablaMenu.Dynamic = true;
            FormatearTablaMenu.Items.Clear();
            // los estilos de tablas los ponemos en el menu
            TablaEstiloTableAdapter daEstilos = new TablaEstiloTableAdapter();
            TablaEstiloDataTable dte = daEstilos.GetData();
            foreach (TablaEstiloRow item in dte.Rows)
            {
                //creamos el boton
                RibbonButton button = this.Factory.CreateRibbonButton();
                button.Label = item.Nombre;
                button.Tag = item.Id;
                button.Click += new RibbonControlEventHandler(TablaEstilo_button_click);
                this.estilosTablas.Add(item.Id, item);
                FormatearTablaMenu.Items.Add(button);
            }

            //--PLANTILLAS TABLAS
            //vaciamos los estilos anteroires siesq hay
            this.plantillasTablas = new Dictionary<int, TablaPlantillaRow>();
            CrearTablaMenu.Dynamic = true;
            CrearTablaMenu.Items.Clear();
            // los estilos de tablas los ponemos en el menu
            TablaPlantillaTableAdapter daPlantillas = new TablaPlantillaTableAdapter();
            TablaPlantillaDataTable dtp = daPlantillas.GetData();
            foreach (TablaPlantillaRow item in dtp.Rows)
            {
                //creamos el boton
                RibbonButton button = this.Factory.CreateRibbonButton();
                button.Label = item.Nombre;
                button.Tag = item.Id;
                button.Click += new RibbonControlEventHandler(TablaPlantilla_button_click);
                this.plantillasTablas.Add(item.Id, item);
                CrearTablaMenu.Items.Add(button);
            }
        }

        void TablaEstilo_button_click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                var btn = (RibbonButton)sender;
                var id = (int)(btn.Tag);
                var sel = Globals.ThisAddIn.Application.Selection;
                sel.Range.Tables[1].set_Style(this.estilosTablas[id].Nombre);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("error al seleccionar estilo " + ex.Message, "Error!");
            }
        }
        void TablaPlantilla_button_click(object sender, RibbonControlEventArgs e)
        {
            try
            {
                var btn = (RibbonButton)sender;
                var id = (int)(btn.Tag);
                var sel = Globals.ThisAddIn.Application.Selection;
                sel.Range.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                var plantilla = this.plantillasTablas[id];
                Word.Table t = sel.Range.Tables.Add(sel.Range, plantilla.Filas, plantilla.Columnas);
                t.set_Style(this.estilosTablas[plantilla.Estilo].Nombre);
                sel.InsertCaption(Word.WdCaptionLabelID.wdCaptionTable, "Titulo Aqui", "", Word.WdCaptionPosition.wdCaptionPositionBelow, false);

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("error al seleccionar estilo " + ex.Message, "Error!");
            }
        }
    }
}
