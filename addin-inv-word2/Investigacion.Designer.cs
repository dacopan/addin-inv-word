﻿namespace addin_inv_word2
{
    partial class Investigacion : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Investigacion()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.menu1 = this.Factory.CreateRibbonMenu();
            this.CambioOrientacion = this.Factory.CreateRibbonButton();
            this.FormatearTablaMenu = this.Factory.CreateRibbonMenu();
            this.group2 = this.Factory.CreateRibbonGroup();
            this.Investigadores = this.Factory.CreateRibbonButton();
            this.Tablas = this.Factory.CreateRibbonButton();
            this.group3 = this.Factory.CreateRibbonGroup();
            this.SelectedInvest = this.Factory.CreateRibbonLabel();
            this.SelectInvest = this.Factory.CreateRibbonButton();
            this.CrearTablaMenu = this.Factory.CreateRibbonMenu();
            this.tab1.SuspendLayout();
            this.group1.SuspendLayout();
            this.group2.SuspendLayout();
            this.group3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Groups.Add(this.group2);
            this.tab1.Groups.Add(this.group3);
            this.tab1.Label = "Invetigacion";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Items.Add(this.menu1);
            this.group1.Items.Add(this.CambioOrientacion);
            this.group1.Items.Add(this.FormatearTablaMenu);
            this.group1.Items.Add(this.CrearTablaMenu);
            this.group1.Label = "Formato";
            this.group1.Name = "group1";
            // 
            // menu1
            // 
            this.menu1.Dynamic = true;
            this.menu1.Label = "Formatear";
            this.menu1.Name = "menu1";
            // 
            // CambioOrientacion
            // 
            this.CambioOrientacion.Label = "Cambiar Orientacion";
            this.CambioOrientacion.Name = "CambioOrientacion";
            this.CambioOrientacion.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.CambioOrientacion_Click);
            // 
            // FormatearTablaMenu
            // 
            this.FormatearTablaMenu.Dynamic = true;
            this.FormatearTablaMenu.Label = "Formatear Tabla";
            this.FormatearTablaMenu.Name = "FormatearTablaMenu";
            // 
            // group2
            // 
            this.group2.Items.Add(this.Investigadores);
            this.group2.Items.Add(this.Tablas);
            this.group2.Label = "Configuraciones";
            this.group2.Name = "group2";
            // 
            // Investigadores
            // 
            this.Investigadores.Label = "Investigadores";
            this.Investigadores.Name = "Investigadores";
            this.Investigadores.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Investigadores_Click);
            // 
            // Tablas
            // 
            this.Tablas.Label = "Tablas";
            this.Tablas.Name = "Tablas";
            this.Tablas.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.Tablas_Click);
            // 
            // group3
            // 
            this.group3.Items.Add(this.SelectedInvest);
            this.group3.Items.Add(this.SelectInvest);
            this.group3.Label = "byDacopanCM J";
            this.group3.Name = "group3";
            // 
            // SelectedInvest
            // 
            this.SelectedInvest.Label = "Sin Investigador";
            this.SelectedInvest.Name = "SelectedInvest";
            // 
            // SelectInvest
            // 
            this.SelectInvest.Label = "Cambiar";
            this.SelectInvest.Name = "SelectInvest";
            this.SelectInvest.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SelectInvest_Click);
            // 
            // CrearTablaMenu
            // 
            this.CrearTablaMenu.Dynamic = true;
            this.CrearTablaMenu.Label = "Crear Tabla";
            this.CrearTablaMenu.Name = "CrearTablaMenu";
            // 
            // Investigacion
            // 
            this.Name = "Investigacion";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Investigacion_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();
            this.group2.ResumeLayout(false);
            this.group2.PerformLayout();
            this.group3.ResumeLayout(false);
            this.group3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Investigadores;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group3;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel SelectedInvest;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SelectInvest;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu menu1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton CambioOrientacion;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton Tablas;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu FormatearTablaMenu;
        internal Microsoft.Office.Tools.Ribbon.RibbonMenu CrearTablaMenu;
    }

    partial class ThisRibbonCollection
    {
        internal Investigacion Investigacion
        {
            get { return this.GetRibbon<Investigacion>(); }
        }
    }
}
