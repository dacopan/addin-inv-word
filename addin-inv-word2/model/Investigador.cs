﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace addin_inv_word2.model
{
    public class Investigador
    {
        private string nombre;
        private string apellido;
        private string fontName;
        private int fontSize;
        private string color;

        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string FontName { get => fontName; set => fontName = value; }
        public int FontSize { get => fontSize; set => fontSize = value; }
        public string Color { get => color; set => color = value; }
    }
}
