﻿namespace addin_inv_word2.forms
{
    partial class FormSelectInvest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboInvest = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ComboInvest
            // 
            this.ComboInvest.FormattingEnabled = true;
            this.ComboInvest.Location = new System.Drawing.Point(115, 60);
            this.ComboInvest.Name = "ComboInvest";
            this.ComboInvest.Size = new System.Drawing.Size(190, 24);
            this.ComboInvest.TabIndex = 0;
            this.ComboInvest.SelectedIndexChanged += new System.EventHandler(this.ComboInvest_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(337, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleccione el investigador para el documento actual";
            // 
            // FormSelectInvest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 283);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboInvest);
            this.Name = "FormSelectInvest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSelectInvest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboInvest;
        private System.Windows.Forms.Label label1;
    }
}