﻿namespace addin_inv_word2
{
    partial class FormInvestigadores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridInvestigadores = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.investigadoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.investuceDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._invest_uceDataSet = new addin_inv_word2._invest_uceDataSet();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.msgInvest = new System.Windows.Forms.Label();
            this.GuardarInvestigadores = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.msgEstilos = new System.Windows.Forms.Label();
            this.gridEstilos = new System.Windows.Forms.DataGridView();
            this.estilosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ComboInvest = new System.Windows.Forms.ComboBox();
            this.EstilosGaurdar = new System.Windows.Forms.Button();
            this.estilosTableAdapter = new addin_inv_word2._invest_uceDataSetTableAdapters.EstilosTableAdapter();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.keyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idinvDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fontDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.fontSizeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.isBoldDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isCursivaDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isSubrayadoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridInvestigadores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.investigadoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.investuceDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._invest_uceDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEstilos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.estilosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridInvestigadores
            // 
            this.gridInvestigadores.AutoGenerateColumns = false;
            this.gridInvestigadores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridInvestigadores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nombreDataGridViewTextBoxColumn});
            this.gridInvestigadores.DataSource = this.investigadoresBindingSource;
            this.gridInvestigadores.Location = new System.Drawing.Point(23, 8);
            this.gridInvestigadores.Name = "gridInvestigadores";
            this.gridInvestigadores.RowTemplate.Height = 24;
            this.gridInvestigadores.Size = new System.Drawing.Size(300, 287);
            this.gridInvestigadores.TabIndex = 1;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            // 
            // investigadoresBindingSource
            // 
            this.investigadoresBindingSource.DataMember = "Investigadores";
            this.investigadoresBindingSource.DataSource = this.investuceDataSetBindingSource;
            // 
            // investuceDataSetBindingSource
            // 
            this.investuceDataSetBindingSource.DataSource = this._invest_uceDataSet;
            this.investuceDataSetBindingSource.Position = 0;
            // 
            // _invest_uceDataSet
            // 
            this._invest_uceDataSet.DataSetName = "_invest_uceDataSet";
            this._invest_uceDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(982, 406);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.msgInvest);
            this.tabPage1.Controls.Add(this.GuardarInvestigadores);
            this.tabPage1.Controls.Add(this.gridInvestigadores);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(612, 358);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Investigadores";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // msgInvest
            // 
            this.msgInvest.AutoSize = true;
            this.msgInvest.Location = new System.Drawing.Point(413, 59);
            this.msgInvest.Name = "msgInvest";
            this.msgInvest.Size = new System.Drawing.Size(0, 17);
            this.msgInvest.TabIndex = 3;
            // 
            // GuardarInvestigadores
            // 
            this.GuardarInvestigadores.Location = new System.Drawing.Point(413, 8);
            this.GuardarInvestigadores.Name = "GuardarInvestigadores";
            this.GuardarInvestigadores.Size = new System.Drawing.Size(75, 23);
            this.GuardarInvestigadores.TabIndex = 2;
            this.GuardarInvestigadores.Text = "Guardar";
            this.GuardarInvestigadores.UseVisualStyleBackColor = true;
            this.GuardarInvestigadores.Click += new System.EventHandler(this.GuardarInvestigadores_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.msgEstilos);
            this.tabPage2.Controls.Add(this.gridEstilos);
            this.tabPage2.Controls.Add(this.ComboInvest);
            this.tabPage2.Controls.Add(this.EstilosGaurdar);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(974, 377);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Estilos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // msgEstilos
            // 
            this.msgEstilos.AutoSize = true;
            this.msgEstilos.Location = new System.Drawing.Point(27, 270);
            this.msgEstilos.Name = "msgEstilos";
            this.msgEstilos.Size = new System.Drawing.Size(0, 17);
            this.msgEstilos.TabIndex = 4;
            // 
            // gridEstilos
            // 
            this.gridEstilos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridEstilos.AutoGenerateColumns = false;
            this.gridEstilos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEstilos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.keyDataGridViewTextBoxColumn,
            this.idinvDataGridViewTextBoxColumn,
            this.fontDataGridViewTextBoxColumn,
            this.fontSizeDataGridViewTextBoxColumn,
            this.colorDataGridViewTextBoxColumn,
            this.isBoldDataGridViewCheckBoxColumn,
            this.isCursivaDataGridViewCheckBoxColumn,
            this.isSubrayadoDataGridViewCheckBoxColumn});
            this.gridEstilos.DataSource = this.estilosBindingSource;
            this.gridEstilos.Location = new System.Drawing.Point(8, 73);
            this.gridEstilos.Name = "gridEstilos";
            this.gridEstilos.RowTemplate.Height = 24;
            this.gridEstilos.Size = new System.Drawing.Size(945, 152);
            this.gridEstilos.TabIndex = 2;
            this.gridEstilos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridEstilos_CellContentClick);
            this.gridEstilos.SelectionChanged += new System.EventHandler(this.gridEstilos_SelectionChanged);
            // 
            // estilosBindingSource
            // 
            this.estilosBindingSource.DataMember = "Estilos";
            this.estilosBindingSource.DataSource = this.investuceDataSetBindingSource;
            // 
            // ComboInvest
            // 
            this.ComboInvest.DisplayMember = "Id";
            this.ComboInvest.FormattingEnabled = true;
            this.ComboInvest.Location = new System.Drawing.Point(8, 24);
            this.ComboInvest.Name = "ComboInvest";
            this.ComboInvest.Size = new System.Drawing.Size(243, 24);
            this.ComboInvest.TabIndex = 1;
            this.ComboInvest.ValueMember = "Id";
            this.ComboInvest.SelectedIndexChanged += new System.EventHandler(this.ComboInvest_SelectedIndexChanged);
            // 
            // EstilosGaurdar
            // 
            this.EstilosGaurdar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.EstilosGaurdar.Location = new System.Drawing.Point(878, 346);
            this.EstilosGaurdar.Name = "EstilosGaurdar";
            this.EstilosGaurdar.Size = new System.Drawing.Size(75, 23);
            this.EstilosGaurdar.TabIndex = 0;
            this.EstilosGaurdar.Text = "Guardar";
            this.EstilosGaurdar.UseVisualStyleBackColor = true;
            this.EstilosGaurdar.Click += new System.EventHandler(this.EstilosGaurdar_Click);
            // 
            // estilosTableAdapter
            // 
            this.estilosTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // keyDataGridViewTextBoxColumn
            // 
            this.keyDataGridViewTextBoxColumn.DataPropertyName = "key";
            this.keyDataGridViewTextBoxColumn.HeaderText = "key";
            this.keyDataGridViewTextBoxColumn.Name = "keyDataGridViewTextBoxColumn";
            // 
            // idinvDataGridViewTextBoxColumn
            // 
            this.idinvDataGridViewTextBoxColumn.DataPropertyName = "id_inv";
            this.idinvDataGridViewTextBoxColumn.HeaderText = "id_inv";
            this.idinvDataGridViewTextBoxColumn.Name = "idinvDataGridViewTextBoxColumn";
            // 
            // fontDataGridViewTextBoxColumn
            // 
            this.fontDataGridViewTextBoxColumn.DataPropertyName = "font";
            this.fontDataGridViewTextBoxColumn.HeaderText = "font";
            this.fontDataGridViewTextBoxColumn.Name = "fontDataGridViewTextBoxColumn";
            this.fontDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fontDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.fontDataGridViewTextBoxColumn.Width = 150;
            // 
            // fontSizeDataGridViewTextBoxColumn
            // 
            this.fontSizeDataGridViewTextBoxColumn.DataPropertyName = "fontSize";
            this.fontSizeDataGridViewTextBoxColumn.HeaderText = "fontSize";
            this.fontSizeDataGridViewTextBoxColumn.Name = "fontSizeDataGridViewTextBoxColumn";
            // 
            // colorDataGridViewTextBoxColumn
            // 
            this.colorDataGridViewTextBoxColumn.DataPropertyName = "color";
            this.colorDataGridViewTextBoxColumn.HeaderText = "color";
            this.colorDataGridViewTextBoxColumn.Name = "colorDataGridViewTextBoxColumn";
            this.colorDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colorDataGridViewTextBoxColumn.Width = 120;
            // 
            // isBoldDataGridViewCheckBoxColumn
            // 
            this.isBoldDataGridViewCheckBoxColumn.DataPropertyName = "isBold";
            this.isBoldDataGridViewCheckBoxColumn.FalseValue = "false";
            this.isBoldDataGridViewCheckBoxColumn.HeaderText = "isBold";
            this.isBoldDataGridViewCheckBoxColumn.IndeterminateValue = "false";
            this.isBoldDataGridViewCheckBoxColumn.Name = "isBoldDataGridViewCheckBoxColumn";
            this.isBoldDataGridViewCheckBoxColumn.TrueValue = "true";
            // 
            // isCursivaDataGridViewCheckBoxColumn
            // 
            this.isCursivaDataGridViewCheckBoxColumn.DataPropertyName = "isCursiva";
            this.isCursivaDataGridViewCheckBoxColumn.FalseValue = "false";
            this.isCursivaDataGridViewCheckBoxColumn.HeaderText = "isCursiva";
            this.isCursivaDataGridViewCheckBoxColumn.IndeterminateValue = "false";
            this.isCursivaDataGridViewCheckBoxColumn.Name = "isCursivaDataGridViewCheckBoxColumn";
            this.isCursivaDataGridViewCheckBoxColumn.TrueValue = "true";
            // 
            // isSubrayadoDataGridViewCheckBoxColumn
            // 
            this.isSubrayadoDataGridViewCheckBoxColumn.DataPropertyName = "isSubrayado";
            this.isSubrayadoDataGridViewCheckBoxColumn.FalseValue = "false";
            this.isSubrayadoDataGridViewCheckBoxColumn.HeaderText = "isSubrayado";
            this.isSubrayadoDataGridViewCheckBoxColumn.IndeterminateValue = "false";
            this.isSubrayadoDataGridViewCheckBoxColumn.Name = "isSubrayadoDataGridViewCheckBoxColumn";
            this.isSubrayadoDataGridViewCheckBoxColumn.TrueValue = "true";
            // 
            // FormInvestigadores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 406);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormInvestigadores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormInvestigadores";
            this.Load += new System.EventHandler(this.FormInvestigadores_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridInvestigadores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.investigadoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.investuceDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._invest_uceDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEstilos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.estilosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridInvestigadores;
        private System.Windows.Forms.BindingSource investuceDataSetBindingSource;
        private _invest_uceDataSet _invest_uceDataSet;
        private System.Windows.Forms.BindingSource investigadoresBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button GuardarInvestigadores;
        private System.Windows.Forms.Label msgInvest;
        private System.Windows.Forms.ComboBox ComboInvest;
        private System.Windows.Forms.Button EstilosGaurdar;
        private System.Windows.Forms.DataGridView gridEstilos;
        private System.Windows.Forms.BindingSource estilosBindingSource;
        private _invest_uceDataSetTableAdapters.EstilosTableAdapter estilosTableAdapter;
        private System.Windows.Forms.Label msgEstilos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn keyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idinvDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn fontDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fontSizeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn colorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isBoldDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isCursivaDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isSubrayadoDataGridViewCheckBoxColumn;
    }
}