﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using addin_inv_word2.model;
using addin_inv_word2._invest_uceDataSetTableAdapters;
using static addin_inv_word2._invest_uceDataSet;
using Word = Microsoft.Office.Interop.Word;

namespace addin_inv_word2
{
    public partial class FormInvestigadores : Form
    {
        private BindingSource bindingInvest = new BindingSource();
        private InvestigadoresTableAdapter daInvest = new InvestigadoresTableAdapter();
        private BindingSource bsInvest;

        private BindingSource bindingEstilos = new BindingSource();
        private EstilosTableAdapter daEstilos = new EstilosTableAdapter();
        BindingSource bsEstilos;

        public FormInvestigadores()
        {
            InitializeComponent();
        }

        private void FormInvestigadores_Load(object sender, EventArgs e)
        {

            ComboInvest.DisplayMember = "nombre";
            ComboInvest.ValueMember = "id";

            // COLORS COMBO BOX
            ComboBox CB = new ComboBox();
            CB.Items.Add(new KeyValuePair<string, string>("Azul", ColorTranslator.ToHtml(Color.Blue)));
            CB.Items.Add(new KeyValuePair<string, string>("Verde", ColorTranslator.ToHtml(Color.Green)));
            CB.Items.Add(new KeyValuePair<string, string>("Rojo", ColorTranslator.ToHtml(Color.Red)));
            CB.Items.Add(new KeyValuePair<string, string>("Amarillo", ColorTranslator.ToHtml(Color.Yellow)));

            foreach (Color color in new ColorConverter().GetStandardValues())
            {
                CB.Items.Add(new KeyValuePair<string, string>(color.Name, ColorTranslator.ToHtml(color)));
            }
            // colors are in 5 index
            var colColumn = ((DataGridViewComboBoxColumn)gridEstilos.Columns[5]);
            colColumn.DataSource = CB.Items;
            colColumn.DisplayMember = "Key";
            colColumn.ValueMember = "Value";


            // FONT COMBO BOX
            ComboBox fontCB = new ComboBox();
            foreach (FontFamily font in FontFamily.Families)
            {
                fontCB.Items.Add(new KeyValuePair<string, string>(font.Name, font.Name));
            }

            var fontColumn = ((DataGridViewComboBoxColumn)gridEstilos.Columns[3]);
            fontColumn.DataSource = fontCB.Items;
            fontColumn.DisplayMember = "Key";
            fontColumn.ValueMember = "Value";

            //grid estilos
            RefreshInvestigadoresList();
            gridEstilos.RowEnter += GridEstilos_RowEnter;
            gridEstilos.DataError += GridEstilos_DataError;
        }

        private void GridEstilos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

            MessageBox.Show(e.Exception.Message + "\n\r en " + gridEstilos.Columns[e.ColumnIndex].HeaderText,
                "Error en los datos");
            msgEstilos.Text = "";
        }

        private void RefreshInvestigadoresList()
        {
            var tbInvest = daInvest.GetData();
            bsInvest = new BindingSource()
            {
                DataSource = tbInvest
            };
            gridInvestigadores.DataSource = bsInvest;
            ComboInvest.DataSource = bsInvest;
        }


        private void GuardarInvestigadores_Click(object sender, EventArgs e)
        {
            msgInvest.Text = "";
            try
            {
                daInvest.Update((InvestigadoresDataTable)bsInvest.DataSource);
                msgInvest.Text = "Actualizado con éxito";
            }
            catch (Exception ex)
            {
                msgInvest.Text = "Error al actualizar" + ex.Message;
            }
        }

        private void ComboInvest_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectId = ((ComboBox)sender).SelectedValue;
            if (selectId != null)
            {
                var tbEstilos = daEstilos.GetDataByInvestigadorId(Convert.ToInt32(selectId));
                bsEstilos = new BindingSource()
                {
                    DataSource = tbEstilos
                };
                gridEstilos.DataSource = bsEstilos;
            }
        }

        private void EstilosGaurdar_Click(object sender, EventArgs e)
        {
            msgEstilos.Text = "";
            try
            {
                daEstilos.Update((EstilosDataTable)bsEstilos.DataSource);
                msgEstilos.Text = "Actualizado con éxito";
            }
            catch (Exception ex)
            {
                msgEstilos.Text = "Error al actualizar" + ex.Message;
            }
        }

        private void GridEstilos_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            check_defaultValues_Estilos(e.RowIndex);
            //var id_invest = gridEstilos.Rows[].Cells[2];
            //if (id_invest.Value == null)
            //{
            //    gridEstilos.Rows[e.RowIndex].Cells[2].Value = ComboInvest.SelectedValue;
            //}
        }

        private void gridEstilos_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow item in gridEstilos.SelectedRows)
            {
                check_defaultValues_Estilos(item.Index);
            }

        }

        private void check_defaultValues_Estilos(int index)
        {
            var id_invest = gridEstilos.Rows[index].Cells[2];
            if (id_invest.Value == null)
            {
                id_invest.Value = ComboInvest.SelectedValue;
            }

            var is_bold = gridEstilos.Rows[index].Cells[6];
            var is_kursiva = gridEstilos.Rows[index].Cells[7];
            var id_subrayado = gridEstilos.Rows[index].Cells[8];

            if (is_bold.Value == null)
            {
                is_bold.Value = false;
            }
            if (is_kursiva.Value == null)
            {
                is_kursiva.Value = false;
            }
            if (id_subrayado.Value == null)
            {
                id_subrayado.Value = false;
            }
        }

        private void gridEstilos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
