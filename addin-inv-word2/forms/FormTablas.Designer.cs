﻿namespace addin_inv_word2
{
    partial class FormTablas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridEstilos = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablaEstiloBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._invest_uceDataSet = new addin_inv_word2._invest_uceDataSet();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.msgEstilos = new System.Windows.Forms.Label();
            this.GuardarEstilos = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.msgPlantillas = new System.Windows.Forms.Label();
            this.gridPlantillas = new System.Windows.Forms.DataGridView();
            this.tablaPlantillaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.GuardarPlantillas = new System.Windows.Forms.Button();
            this.tablaEstiloTableAdapter = new addin_inv_word2._invest_uceDataSetTableAdapters.TablaEstiloTableAdapter();
            this.tablaPlantillaTableAdapter = new addin_inv_word2._invest_uceDataSetTableAdapters.TablaPlantillaTableAdapter();
            this.idDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estiloDataGridViewComboBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridEstilos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaEstiloBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._invest_uceDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlantillas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaPlantillaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gridEstilos
            // 
            this.gridEstilos.AutoGenerateColumns = false;
            this.gridEstilos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridEstilos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nombreDataGridViewTextBoxColumn});
            this.gridEstilos.DataSource = this.tablaEstiloBindingSource;
            this.gridEstilos.Location = new System.Drawing.Point(23, 8);
            this.gridEstilos.Name = "gridEstilos";
            this.gridEstilos.RowTemplate.Height = 24;
            this.gridEstilos.Size = new System.Drawing.Size(300, 287);
            this.gridEstilos.TabIndex = 1;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            // 
            // tablaEstiloBindingSource
            // 
            this.tablaEstiloBindingSource.DataMember = "TablaEstilo";
            this.tablaEstiloBindingSource.DataSource = this._invest_uceDataSet;
            // 
            // _invest_uceDataSet
            // 
            this._invest_uceDataSet.DataSetName = "_invest_uceDataSet";
            this._invest_uceDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(982, 406);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.msgEstilos);
            this.tabPage1.Controls.Add(this.GuardarEstilos);
            this.tabPage1.Controls.Add(this.gridEstilos);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(974, 377);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Estilos";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // msgEstilos
            // 
            this.msgEstilos.AutoSize = true;
            this.msgEstilos.Location = new System.Drawing.Point(413, 59);
            this.msgEstilos.Name = "msgEstilos";
            this.msgEstilos.Size = new System.Drawing.Size(0, 17);
            this.msgEstilos.TabIndex = 3;
            // 
            // GuardarEstilos
            // 
            this.GuardarEstilos.Location = new System.Drawing.Point(413, 8);
            this.GuardarEstilos.Name = "GuardarEstilos";
            this.GuardarEstilos.Size = new System.Drawing.Size(75, 23);
            this.GuardarEstilos.TabIndex = 2;
            this.GuardarEstilos.Text = "Guardar";
            this.GuardarEstilos.UseVisualStyleBackColor = true;
            this.GuardarEstilos.Click += new System.EventHandler(this.GuardarEstilos_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.msgPlantillas);
            this.tabPage2.Controls.Add(this.gridPlantillas);
            this.tabPage2.Controls.Add(this.GuardarPlantillas);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(974, 377);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tablas";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // msgPlantillas
            // 
            this.msgPlantillas.AutoSize = true;
            this.msgPlantillas.Location = new System.Drawing.Point(27, 270);
            this.msgPlantillas.Name = "msgPlantillas";
            this.msgPlantillas.Size = new System.Drawing.Size(0, 17);
            this.msgPlantillas.TabIndex = 4;
            // 
            // gridPlantillas
            // 
            this.gridPlantillas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridPlantillas.AutoGenerateColumns = false;
            this.gridPlantillas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPlantillas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn1,
            this.nombreDataGridViewTextBoxColumn1,
            this.filasDataGridViewTextBoxColumn,
            this.columnasDataGridViewTextBoxColumn,
            this.estiloDataGridViewComboBoxColumn});
            this.gridPlantillas.DataSource = this.tablaPlantillaBindingSource;
            this.gridPlantillas.Location = new System.Drawing.Point(8, 73);
            this.gridPlantillas.Name = "gridPlantillas";
            this.gridPlantillas.RowTemplate.Height = 24;
            this.gridPlantillas.Size = new System.Drawing.Size(945, 152);
            this.gridPlantillas.TabIndex = 2;
            // 
            // tablaPlantillaBindingSource
            // 
            this.tablaPlantillaBindingSource.DataMember = "TablaPlantilla";
            this.tablaPlantillaBindingSource.DataSource = this._invest_uceDataSet;
            // 
            // GuardarPlantillas
            // 
            this.GuardarPlantillas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.GuardarPlantillas.Location = new System.Drawing.Point(878, 346);
            this.GuardarPlantillas.Name = "GuardarPlantillas";
            this.GuardarPlantillas.Size = new System.Drawing.Size(75, 23);
            this.GuardarPlantillas.TabIndex = 0;
            this.GuardarPlantillas.Text = "Guardar";
            this.GuardarPlantillas.UseVisualStyleBackColor = true;
            this.GuardarPlantillas.Click += new System.EventHandler(this.GuardarPlantillas_Click);
            // 
            // tablaEstiloTableAdapter
            // 
            this.tablaEstiloTableAdapter.ClearBeforeFill = true;
            // 
            // tablaPlantillaTableAdapter
            // 
            this.tablaPlantillaTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn1
            // 
            this.idDataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn1.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn1.Name = "idDataGridViewTextBoxColumn1";
            this.idDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // nombreDataGridViewTextBoxColumn1
            // 
            this.nombreDataGridViewTextBoxColumn1.DataPropertyName = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn1.Name = "nombreDataGridViewTextBoxColumn1";
            // 
            // filasDataGridViewTextBoxColumn
            // 
            this.filasDataGridViewTextBoxColumn.DataPropertyName = "Filas";
            this.filasDataGridViewTextBoxColumn.HeaderText = "Filas";
            this.filasDataGridViewTextBoxColumn.Name = "filasDataGridViewTextBoxColumn";
            // 
            // columnasDataGridViewTextBoxColumn
            // 
            this.columnasDataGridViewTextBoxColumn.DataPropertyName = "Columnas";
            this.columnasDataGridViewTextBoxColumn.HeaderText = "Columnas";
            this.columnasDataGridViewTextBoxColumn.Name = "columnasDataGridViewTextBoxColumn";
            // 
            // estiloDataGridViewComboBoxColumn
            // 
            this.estiloDataGridViewComboBoxColumn.DataPropertyName = "Estilo";
            this.estiloDataGridViewComboBoxColumn.HeaderText = "Estilo";
            this.estiloDataGridViewComboBoxColumn.Name = "estiloDataGridViewComboBoxColumn";
            this.estiloDataGridViewComboBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.estiloDataGridViewComboBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // FormTablas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 406);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormTablas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormInvestigadores";
            this.Load += new System.EventHandler(this.FormTablas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridEstilos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaEstiloBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._invest_uceDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPlantillas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablaPlantillaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView gridEstilos;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button GuardarEstilos;
        private System.Windows.Forms.Label msgEstilos;
        private System.Windows.Forms.Button GuardarPlantillas;
        private System.Windows.Forms.DataGridView gridPlantillas;
        private System.Windows.Forms.Label msgPlantillas;
        private _invest_uceDataSet _invest_uceDataSet;
        private System.Windows.Forms.BindingSource tablaEstiloBindingSource;
        private _invest_uceDataSetTableAdapters.TablaEstiloTableAdapter tablaEstiloTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource tablaPlantillaBindingSource;
        private _invest_uceDataSetTableAdapters.TablaPlantillaTableAdapter tablaPlantillaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn filasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnasDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn estiloDataGridViewComboBoxColumn;
    }
}