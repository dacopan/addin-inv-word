﻿using addin_inv_word2._invest_uceDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace addin_inv_word2.forms
{
    public partial class FormSelectInvest : Form
    {
        private BindingSource bindingInvest = new BindingSource();
        private InvestigadoresTableAdapter daInvest = new InvestigadoresTableAdapter();
        private BindingSource bsInvest;

        public FormSelectInvest()
        {
            InitializeComponent();
            this.Load += FormSelectInvest_Load;

        }

        private void FormSelectInvest_Load(object sender, EventArgs e)
        {
            ComboInvest.DisplayMember = "nombre";
            ComboInvest.ValueMember = "id";
            RefreshInvestigadoresList();
        }

        private void RefreshInvestigadoresList()
        {
            var tbInvest = daInvest.GetData();
            bsInvest = new BindingSource()
            {
                DataSource = tbInvest
            };
            ComboInvest.DataSource = bsInvest;
            if (tbInvest.Rows.Count > 0)
            {
                Globals.ThisAddIn.IdInvest = Convert.ToInt32(tbInvest.Rows[0]["Id"]);
            }
        }


        private void ComboInvest_SelectedIndexChanged(object sender, EventArgs e)
        {
            Globals.ThisAddIn.IdInvest = Convert.ToInt32(ComboInvest.SelectedValue);
        }
    }
}
