﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using addin_inv_word2.model;
using addin_inv_word2._invest_uceDataSetTableAdapters;
using static addin_inv_word2._invest_uceDataSet;
using Word = Microsoft.Office.Interop.Word;

namespace addin_inv_word2
{
    public partial class FormTablas : Form
    {
        private BindingSource bindingEstilos = new BindingSource();
        private TablaEstiloTableAdapter daEstilos = new TablaEstiloTableAdapter();
        private BindingSource bsEstilos;

        private BindingSource bindingPlantillas = new BindingSource();
        private TablaPlantillaTableAdapter daPlantillas = new TablaPlantillaTableAdapter();
        BindingSource bsPlantillas;

        public FormTablas()
        {
            InitializeComponent();
        }

        private void FormTablas_Load(object sender, EventArgs e)
        {
            RefreshEstilosList();
            RefreshPlantillasList();
            gridPlantillas.DataError += GridEstilos_DataError;
        }

        private void GridEstilos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message + "\n\r en " + gridPlantillas.Columns[e.ColumnIndex].HeaderText,
                "Error en los datos");
            msgPlantillas.Text = "";
        }

        private void RefreshEstilosList()
        {
            var tbEstilos = daEstilos.GetData();
            bsEstilos = new BindingSource()
            {
                DataSource = tbEstilos
            };
            gridEstilos.DataSource = bsEstilos;

            // ESTILOS COMBO BOX         
            estiloDataGridViewComboBoxColumn.DataSource = bsEstilos;
            estiloDataGridViewComboBoxColumn.DisplayMember = "Nombre";
            estiloDataGridViewComboBoxColumn.ValueMember = "Id";
        }

        private void RefreshPlantillasList()
        {
            var tbPlantillas = daPlantillas.GetData();
            bsPlantillas = new BindingSource()
            {
                DataSource = tbPlantillas
            };
            gridPlantillas.DataSource = bsPlantillas;
        }

        private void GuardarPlantillas_Click(object sender, EventArgs e)
        {
            msgPlantillas.Text = "";
            try
            {
                daPlantillas.Update((TablaPlantillaDataTable)bsPlantillas.DataSource);
                msgPlantillas.Text = "Actualizado con éxito";
            }
            catch (Exception ex)
            {
                msgPlantillas.Text = "Error al actualizar" + ex.Message;
            }
        }

        private void GuardarEstilos_Click(object sender, EventArgs e)
        {
            msgEstilos.Text = "";
            try
            {
                daEstilos.Update((TablaEstiloDataTable)bsEstilos.DataSource);
                msgEstilos.Text = "Actualizado con éxito";
            }
            catch (Exception ex)
            {
                msgEstilos.Text = "Error al actualizar" + ex.Message;
            }
        }
    }
}
